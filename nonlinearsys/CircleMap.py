#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


class Circmap():
    def __init__(self, K=1, Om=0.3, x0=None):
        self.K = K
        self.Om = Om
        if x0 is None:
            self.x0 = np.random.uniform(0.00001, 1, 1)
        else:
            self.x0 = x0

    def run(self, N=10000):
        N = N + 5000
        x = np.zeros(N)
        x[0] = self.x0
        for i in range(1, N):
            x[i] = (x[i - 1] + self.Om
            - (self.K / (2 * np.pi)) * np.sin(2 * np.pi * x[i - 1])) % 1

        return x[4999:-1]


if __name__ == '__main__':
    Cmap = Circmap(Om=0.7)
    x = Cmap.run()
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharey=True)
    ax1.plot(x[1:-2], x[2:-1], 'r.')
    ax2.plot(x[0:50])
    plt.suptitle('Circle Map')
    plt.show()
