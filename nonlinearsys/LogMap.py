#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


class Logmap():
    def __init__(self, r=3.98, x0=None):
        self.r = r
        if x0 is None:
            self.x0 = np.random.uniform(0.00001, 1, 1)
        else:
            self.x0 = x0

    def run(self, N=10000):
        N = N + 5000
        x = np.zeros(N)
        x[0] = self.x0
        for i in range(1, N):
            x[i] = self.r * x[i - 1] * (1 - x[i - 1])

        return x[4999:-1]


if __name__ == '__main__':
    Lmap = Logmap()
    x = Lmap.run()
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharey=True)
    ax1.plot(x[1:-2], x[2:-1], 'r.')
    ax2.plot(x[0:50])
    plt.suptitle('Logistic Map')
    plt.show()
