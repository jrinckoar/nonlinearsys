#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


# https://en.wikipedia.org/wiki/Hindmarsh%E2%80%93Rose_model
# https://sci-hub.bz/http://dx.doi.org/10.1063/1.2975967
class HindmarshRose():
    def __init__(self, a=1, b=3, c=1, d=5, s=4, xr=-8 / 5, r=0.01, I=2,
                 x0=[None, None, None]):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.s = s
        self.r = r
        self.I = I
        self.xr = xr
        if None in x0:
            self.x0 = np.random.uniform(-1, 1, 3)
        else:
            self.x0 = x0

    def run(self, N=10000, dt=0.1):
        N = N + 5000
        tf = dt * N
        t = np.arange(0, tf, dt)
        sol = odeint(fun, self.x0, t, args=(self.a, self.b, self.c, self.d,
                     self.s, self.xr, self.r, self.I))
        return sol[4999:-1, :]


def fun(state, t, a, b, c, d, s, xr, r, I):
    x, y, z = state
    phi = -a * x**3 + b * x**2
    psi = c - d * x**2
    return y + phi - z + I, psi - y, r * (s * (x - xr) - z)


if __name__ == '__main__':
    HRsys = HindmarshRose()
    sol = HRsys.run()
    x = sol[:, 0]
    y = sol[:, 1]
    z = sol[:, 2]
    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((3, 2), (0, 0))
    ax2 = plt.subplot2grid((3, 2), (1, 0), sharex=ax1)
    ax3 = plt.subplot2grid((3, 2), (2, 0), sharex=ax1)
    ax4 = plt.subplot2grid((3, 2), (0, 1), rowspan=3, projection='3d')
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)

    ax1.plot(x[0:5000])
    ax2.plot(y[0:5000])
    ax3.plot(z[0:5000])
    ax4.plot(x, y, z, 'r')

    ax1.set_ylabel('x(t)')
    ax2.set_ylabel('y(t)')
    ax3.set_ylabel('z(t)')
    ax3.set_xlabel('t')
    ax4.set_xlabel('x')
    ax4.set_ylabel('y')
    ax4.set_zlabel('z')

    plt.suptitle('Hindmarsh - Rose System')
    plt.show()
