#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# http://www.scholarpedia.org/article/Hyperchaos
# Chaos Detection and Predictability

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


class FoldedTowelmap():
    def __init__(self, a=3.8, b=0.2, x0=[None, None, None]):
        self.a = a
        self.b = b
        self.x0 = np.zeros(3)
        if None in x0:
            self.x0[0] = np.random.uniform(0, 1, 1)
            self.x0[1] = np.random.uniform(-0.1, 0.1, 1)
            self.x0[2] = np.random.uniform(0, 1, 1)
        else:
            self.x0 = x0

    def run(self, N=10000):
        N = N + 5000
        x = np.zeros(N)
        y = np.zeros(N)
        z = np.zeros(N)
        x[0] = self.x0[0]
        y[0] = self.x0[1]
        z[0] = self.x0[2]
        print('{0} {1} {2}'.format(x[0],y[0],z[0]))
        for i in range(1, N):
            xn = x[i - 1]
            yn = y[i - 1]
            zn = z[i - 1]
            x[i] = self.a * xn * (1 - xn) - 0.05 * (yn + 0.35) * (1 - 2 * zn)
            y[i] = 0.1 * ((yn + 0.35) * (1 - 2 * zn) - 1) * (1 - 1.9 * xn)
            z[i] = 3.78 * zn * (1 - zn) + self.b * yn
            if i < 10:
                print('{0} {1} {2}'.format(x[i],y[i],z[i]))

        return x[4999:-1], y[4999:-1], z[4999:-1]


if __name__ == '__main__':
    FTowel = FoldedTowelmap()
    x, y, z = FTowel.run()
    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((3, 3), (0, 0))
    ax2 = plt.subplot2grid((3, 3), (1, 0), sharex=ax1)
    ax3 = plt.subplot2grid((3, 3), (2, 0), sharex=ax1)
    ax4 = plt.subplot2grid((3, 3), (0, 1), rowspan=3, projection='3d')
    ax5 = plt.subplot2grid((3, 3), (0, 2), rowspan=3)
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)

    ax1.plot(x[0:100])
    ax2.plot(y[0:100])
    ax3.plot(z[0:100])
    ax4.plot(x, y, z, 'r.')
    ax5.plot(y[1:-2], y[2:-1], 'k.')

    ax1.set_ylabel('x(n)')
    ax2.set_ylabel('y(n)')
    ax3.set_ylabel('z(n)')
    ax3.set_xlabel('t')
    ax4.set_xlabel('x')
    ax4.set_ylabel('y')
    ax4.set_zlabel('z')

    plt.suptitle('Folded-Towel Map')
    plt.show()
