#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


class KaplanYorkmap():
    def __init__(self, alpha=0.2, x0=[None, None]):
        self.alpha = alpha
        self.a0 = 5
        self.b = 4373            # Large prime number
        if None in x0:
            self.x0 = np.random.uniform(0.00001, 1, 2)
        else:
            self.x0 = x0

    def run(self, N=10000):
        N = N + 5000
        x = np.zeros(N)
        a = np.zeros(N)
        y = np.zeros(N)
        a[0] = self.a0
        x[0] = self.x0[0]
        y[0] = self.x0[1]
        for i in range(1, N):
            a[i] = (2 * a[i - 1]) % self.b
            x[i] = a[i - 1] / self.b
            y[i] = self.alpha * y[i - 1] + np.cos(4 * np.pi * x[i - 1])
        return x[4999:-1], y[4999:-1]


if __name__ == '__main__':
    KYmap = KaplanYorkmap(x0=[128873 / 350377, 0.667751])
    x, y = KYmap.run()
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharey=True)
    ax1.plot(x[1:-1], y[1:-1], 'r.')
    ax2.plot(x[0:50])
    plt.suptitle('Kaplan York Map')
    plt.show()
