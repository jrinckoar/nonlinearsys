#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# http://www.sciencedirect.com/science/article/pii/S0096300304006988
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


class HyperChen():
    def __init__(self, a=35, b=3, c=12, d=7, r=0.5,
                 x0=[None, None, None, None]):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.r = r
        if None in x0:
            self.x0 = np.random.uniform(-10, 10, 4)
        else:
            self.x0 = x0

    def run(self, N=10000, dt=0.005):
        N = N + 5000
        tf = dt * N
        t = np.arange(0, tf, dt)
        sol = odeint(fun, self.x0, t, args=(self.a, self.b, self.c, self.d,
                     self.r))
        return sol[4999:-1, :]


def fun(state, t, a, b, c, d, r):
    x, y, z, w = state
    return a * (y - x) + w, d * x + c * y - x * z, x * y - b * z, y * z + r * w


if __name__ == '__main__':
    HpChensys = HyperChen()
    sol = HpChensys.run()
    x = sol[:, 0]
    y = sol[:, 1]
    z = sol[:, 2]
    w = sol[:, 3]
    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((4, 2), (0, 0))
    ax2 = plt.subplot2grid((4, 2), (1, 0), sharex=ax1)
    ax3 = plt.subplot2grid((4, 2), (2, 0), sharex=ax1)
    ax4 = plt.subplot2grid((4, 2), (3, 0), sharex=ax1)
    ax5 = plt.subplot2grid((4, 2), (0, 1), rowspan=4, projection='3d')
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)
    plt.setp(ax3.get_xticklabels(), visible=False)

    ax1.plot(x[0:1000])
    ax2.plot(y[0:1000])
    ax3.plot(z[0:1000])
    ax4.plot(w[0:1000])
    ax5.plot(y, z, w, 'r')

    ax1.set_ylabel('x(t)')
    ax2.set_ylabel('y(t)')
    ax3.set_ylabel('z(t)')
    ax4.set_ylabel('w(t)')
    ax4.set_xlabel('t')
    ax5.set_xlabel('y')
    ax5.set_ylabel('z')
    ax5.set_zlabel('w')

    plt.suptitle('Hyperchaotic Chen System')
    plt.show()






