#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


class Lorenz():
    def __init__(self, sigma=10, beta=8 / 3, rho=28, x0=[None, None, None]):
        self.sigma = sigma
        self.beta = beta
        self.rho = rho
        if None in x0:
            self.x0 = np.random.uniform(-1, 1, 3)
        else:
            self.x0 = x0

    def run(self, N=10000, dt=0.02):
        N = N + 5000
        tf = dt * N
        t = np.arange(0, tf, dt)
        sol = odeint(fun, self.x0, t, args=(self.sigma, self.beta, self.rho))
        return sol[4999:-1, :]


def fun(state, t, sigma, beta, rho):
    x, y, z = state
    return sigma * (y - x), x * (rho - z), x * y - beta * z


if __name__ == '__main__':
    Lorsys = Lorenz()
    sol = Lorsys.run()
    x = sol[:, 0]
    y = sol[:, 1]
    z = sol[:, 2]
    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((3, 2), (0, 0))
    ax2 = plt.subplot2grid((3, 2), (1, 0), sharex=ax1)
    ax3 = plt.subplot2grid((3, 2), (2, 0), sharex=ax1)
    ax4 = plt.subplot2grid((3, 2), (0, 1), rowspan=3, projection='3d')
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)

    ax1.plot(x[0:1000])
    ax2.plot(y[0:1000])
    ax3.plot(z[0:1000])
    ax4.plot(x, y, z, 'r')

    ax1.set_ylabel('x(t)')
    ax2.set_ylabel('y(t)')
    ax3.set_ylabel('z(t)')
    ax3.set_xlabel('t')
    ax4.set_xlabel('x')
    ax4.set_ylabel('y')
    ax4.set_zlabel('z')

    plt.suptitle('Lorenz System')
    plt.show()
