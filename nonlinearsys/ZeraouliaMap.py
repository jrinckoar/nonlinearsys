#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# http://sprott.physics.wisc.edu/chaos/elhadj/2dmap4/pub4.pdf
import matplotlib.pyplot as plt
import numpy as np


class Zeraouliamap():
    def __init__(self, a=4, b=0.9, x0=[None, None]):
        self.a = a
        self.b = b
        if None in x0:
            self.x0 = np.random.uniform(0.00001, 1, 2)
        else:
            self.x0 = x0

    def run(self, N=10000):
        N = N + 5000
        x = np.zeros(N)
        y = np.zeros(N)
        x[0] = self.x0[0]
        y[0] = self.x0[1]
        for i in range(1, N):
            x[i] = -self.a * x[i - 1] / (1 + y[i - 1]**2)
            y[i] = x[i - 1] + self.b * y[i - 1]
        return x[4999:-1], y[4999:-1]


if __name__ == '__main__':
    ZEmap = Zeraouliamap(a=3.7, b=0.6)
    x, y = ZEmap.run()
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharey=True)
    ax1.plot(x[1:-1], y[1:-1], 'r.')
    ax2.plot(x[0:50])
    plt.suptitle('Zeraoulia Map')
    plt.show()
