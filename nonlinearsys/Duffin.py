#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# https://en.wikipedia.org/wiki/Duffing_equation
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


class Duffin():
    def __init__(self, alpha=-1, beta=1, delta=0.3, gamma=0.37, omega=1.2,
                 x0=[None, None]):
        self.alpha = alpha
        self.beta = beta
        self.delta = delta
        self.gamma = gamma
        self.omega = omega
        if None in x0:
            self.x0 = np.random.uniform(-1.5, 1.5, 2)
        else:
            self.x0 = x0

    def run(self, N=10000, dt=0.02):
        N = N + 5000
        tf = dt * N
        t = np.arange(0, tf, dt)
        sol = odeint(fun, self.x0, t, args=(self.alpha, self.beta, self.delta,
                     self.gamma, self.omega))
        return sol[4999:-1, :]


def fun(state, t, alpha, beta, delta, gamma, omega):
    x, y = state
    return y, - alpha * x - beta * x**3 - delta * y + gamma * np.cos(omega * t)


if __name__ == '__main__':
    Duffinsys = Duffin(gamma=0.37)
    sol = Duffinsys.run()
    x = sol[:, 0]
    y = sol[:, 1]
    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((2, 2), (0, 0))
    ax2 = plt.subplot2grid((2, 2), (1, 0), sharex=ax1)
    ax3 = plt.subplot2grid((2, 2), (0, 1), rowspan=2)
    plt.setp(ax1.get_xticklabels(), visible=False)

    ax1.plot(x[0:1000])
    ax2.plot(y[0:1000])
    ax3.plot(x, y, 'r')

    ax1.set_ylabel('x(t)')
    ax2.set_ylabel('y(t)')
    ax2.set_xlabel('t')
    ax3.set_xlabel('x')
    ax3.set_ylabel('y')

    plt.suptitle('Duffin System')
    plt.show()
