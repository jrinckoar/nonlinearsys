#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import numpy as np
import matplotlib.pyplot as plt


class Henonmap():
    def __init__(self, a=1.4, b=0.3):
        self.a = a
        self.b = b

    def run(self, N=10000, x0=[None, None]):
        if None in x0:
            x0 = np.random.uniform(0.1, 1, 2)
        conv_flag = True
        N = N + 5000
        x = np.zeros(N)
        y = np.zeros(N)
        while conv_flag:
            x[0] = x0[0]
            y[0] = x0[1]
            for i in range(1, N):
                x[i] = 1 - self.a * (x[i - 1]**2) + y[i - 1]
                y[i] = self.b * x[i - 1]
            if np.isinf(x).any():
                sys.stderr.write('changing initial condition')
                x0 = np.random.uniform(0.1, 1, 2)
            else:
                conv_flag = False
        return x[4999:-1], y[4999:-1]


if __name__ == '__main__':
    Hmap = Henonmap()
    x, y = Hmap.run()
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharey=True)
    ax1.plot(x[1:-1], y[1:-1], 'r.')
    ax2.plot(x[0:50])
    plt.suptitle('Henon Map')
    plt.show()
