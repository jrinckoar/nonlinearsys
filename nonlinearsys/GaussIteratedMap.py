#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


class GaussIteratedmap():
    def __init__(self, alpha=4.90, beta=-0.58, x0=None):
        self.alpha = alpha
        self.beta = beta
        if x0 is None:
            self.x0 = np.random.uniform(-1, 1, 1)
        else:
            self.x0 = x0

    def run(self, N=10000):
        N = N + 5000
        x = np.zeros(N)
        x[0] = self.x0
        for i in range(1, N):
            x[i] = np.exp(-self.alpha * x[i - 1]**2) + self.beta
        return x[4999:-1]


if __name__ == '__main__':
    GImap = GaussIteratedmap()
    x = GImap.run()
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, sharey=True)
    ax1.plot(x[1:-2], x[2:-1], 'r.')
    ax2.plot(x[0:50])
    plt.suptitle('Gauss Iterated Map')
    plt.show()
