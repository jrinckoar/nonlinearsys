#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scipy.integrate import odeint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


class Rossler():
    def __init__(self, a=0.2, b=0.2, c=5.7, x0=[None, None, None]):
        self.a = a
        self.b = b
        self.c = c
        if None in x0:
            self.x0 = np.random.uniform(-1, 1, 3)
        else:
            self.x0 = x0

    def run(self, N=10000, dt=0.02):
        N = N + 5000
        tf = dt * N
        t = np.arange(0, tf, dt)
        sol = odeint(fun, self.x0, t, args=(self.a, self.b, self.c))
        return sol[4999:-1, :]


def fun(state, t, a, b, c):
    x, y, z = state
    return -y - z, x + a * y, b + z * (x - c)


if __name__ == '__main__':
    Rossys = Rossler()
    sol = Rossys.run()
    x = sol[:, 0]
    y = sol[:, 1]
    z = sol[:, 2]
    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((3, 2), (0, 0))
    ax2 = plt.subplot2grid((3, 2), (1, 0), sharex=ax1)
    ax3 = plt.subplot2grid((3, 2), (2, 0), sharex=ax1)
    ax4 = plt.subplot2grid((3, 2), (0, 1), rowspan=3, projection='3d')
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)

    ax1.plot(x[0:1000])
    ax2.plot(y[0:1000])
    ax3.plot(z[0:1000])
    ax4.plot(x, y, z, 'r')

    ax1.set_ylabel('x(t)')
    ax2.set_ylabel('y(t)')
    ax3.set_ylabel('z(t)')
    ax3.set_xlabel('t')
    ax4.set_xlabel('x')
    ax4.set_ylabel('y')
    ax4.set_zlabel('z')

    plt.suptitle('Rössler System')
    plt.show()
