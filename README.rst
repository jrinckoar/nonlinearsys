Nonlinearsys
------------

Compilation of python routines to solve nonlinear equation systems.

Continuous systems:
* ChuaCircuit
* Duffin
* Lorenz  
* Rössler 
* Hyperchaotic Rössler
* Hyperchaotic Chen 
* Hindmarsh-Rose model.

Discreate systems:
* Logistic map.
* Henon map.
* Tent map.
* Zeraolia map.
* Kaplan-York mao
* Circle map.
* Folded towel map.
* Gauss iterated map.
