#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='nonlinearsys',
      version='0.1',
      description='Compilation of Nonlinear systems',
      long_description=readme(),
      url='https://jrinckoar@bitbucket.org/jrinckoar/nonlinearsys.git',
      author='Juan F. Restrepo',
      author_email='jrestrepo@bioingenieria.edu.ar',
      license='MIT',
      packages=['nonlinearsys'],
      keywords='nonlinear-dynamics nonlinear-system',
      install_requires=[
          'scipy',
          'numpy',
          'matplotlib',
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      zip_safe=False)
